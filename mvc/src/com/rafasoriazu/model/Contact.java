/* 
 * Copyright (c) Curso Introducción Spring Famework2015 
  * Code: https://bitbucket.org/rafassmail/cursospring
 */

package com.rafasoriazu.model;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Contact model class
 * 
 * @author: Rafael Soriazu (rafasoriazu@gmail.com)
 */

public class Contact {

	 @NotNull
	 @Min(value = 1)
	 private Long id;
	 @Email
	 @NotBlank
	 private String email;
	 @NotBlank
	 private String name;


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
