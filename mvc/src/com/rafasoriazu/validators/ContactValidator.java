package com.rafasoriazu.validators;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.rafasoriazu.model.Contact;

public class ContactValidator implements Validator {

	@Override
	public boolean supports(Class<?> arg0) {
		return Contact.class.equals(arg0);
	}

	@Override
	public void validate(Object obj, Errors e) {
		ValidationUtils.rejectIfEmptyOrWhitespace(e, "id", "id.empty");
        Contact c = (Contact) obj;
        if (c.getId()!=null){
        	if (c.getId() < 0) {
        		e.rejectValue("id", "id.negativevalue");
        	}
        }
	}

}
